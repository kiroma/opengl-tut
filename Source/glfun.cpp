#include "glfun.hpp"
#include <sstream>
#include <iostream>
#include <fstream>

template<typename T>
std::vector<T> parse_vertices(const char* vertices_path)
{
	std::vector<T> vertices{};
	std::fstream fs(vertices_path, std::ios::binary | std::ios::in);
	for(T i; fs >> i;)
	{
		vertices.push_back(i);
	}
	return vertices;
}

GLuint load_shader(const char* shader_path, GLenum shaderType)
{
	std::fstream fs(shader_path, std::ios::binary | std::ios::in);
	std::stringstream buffer;
	buffer << fs.rdbuf();
	std::string shader(buffer.str());
	GLuint shaderEnum = glCreateShader(shaderType);
	const GLchar *shaderSource = static_cast<const GLchar*>(shader.c_str());
	glShaderSource(shaderEnum, 1, &shaderSource, NULL);
	glCompileShader(shaderEnum );
	GLint status;
	glGetShaderiv(shaderEnum, GL_COMPILE_STATUS, &status);
	if(status != GL_TRUE)
	{
		std::cout << "Error, failed to compile the vertex Shader" << std::endl;
		char info[512];
		glGetShaderInfoLog(shaderEnum, 512, NULL, info);
		std::cout << "Shader log: " << info << std::endl;
	}
	return shaderEnum;
}

vao_t::vao_t(GLsizei array_num) : buffnum(array_num)
{
	glGenVertexArrays(array_num, &this->buffenum);
}

vao_t::vao_t(vao_t&& source) : buffenum(source.buffenum), buffnum(source.buffnum)
{
	source.buffnum = 0;
	source.buffenum = 0;
}

vao_t::~vao_t()
{
	glDeleteVertexArrays(this->buffnum, &this->buffenum);
}

void vao_t::bind()
{
	glBindVertexArray(this->buffenum);
}

vbo_t::vbo_t(GLsizei array_num) : buffnum(array_num)
{
	glGenBuffers(array_num, &this->buffenum);
}

vbo_t::vbo_t(vbo_t && source) : vertices(std::move(source.vertices)), buffenum(source.buffenum), buffnum(source.buffnum)
{
	source.buffnum = 0;
	source.buffenum = 0;
}

vbo_t::~vbo_t()
{
	glDeleteBuffers(this->buffnum, &this->buffenum);
}

void vbo_t::load_vertices(const char* path)
{
	this->vertices = parse_vertices<GLfloat>(path);
}

void vbo_t::bind()
{
	glBindBuffer(GL_ARRAY_BUFFER, this->buffenum);
}

void vbo_t::buffer_data(GLenum usage)
{
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size()*sizeof(GLfloat), this->vertices.data(), usage);
}

ebo_t::ebo_t(GLsizei array_num) : buffnum(array_num)
{
	glGenBuffers(array_num, &this->buffenum);
}

ebo_t::ebo_t(ebo_t && source) : elements(std::move(source.elements)), buffenum(source.buffenum), buffnum(source.buffnum)
{
	source.buffenum = 0;
	source.buffnum = 0;
}

ebo_t::~ebo_t()
{
	glDeleteBuffers(this->buffnum, &this->buffenum);
}

void ebo_t::bind()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->buffenum);
}

void ebo_t::load_elements(const char* path)
{
	this->elements = parse_vertices<GLuint>(path);
}

void ebo_t::buffer_data(GLenum usage)
{
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size()*sizeof(GLuint), elements.data(), usage);
}
