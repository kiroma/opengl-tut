#pragma once
#include <GL/glew.h>
#include <png.h>
#include <vector>
#include <initializer_list>

struct image_t
{
	image_t(std::initializer_list<const char*> paths);
	~image_t();
	png_uint_32 width, height;
	int color_type, bit_depth, interlace_type;
	std::vector<GLuint> name;
private:
	void abort_(const char* error_message);
	png_structp png_ptr = nullptr;
	png_infop info_ptr = nullptr;
	png_infop end_info = nullptr;
	png_byte * image_data;
	png_byte ** row_pointers;
	GLint format;
};
