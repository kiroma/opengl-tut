#include "init.hpp"
#include "glfun.hpp"
#include "texfun.hpp"
#include <iostream>
#include <chrono>

int main(int argc, char* argv[])
{
	window_t m_window(SDL_INIT_VIDEO | SDL_INIT_EVENTS, "hullo", 640, 480, SDL_WINDOW_OPENGL);
	if(argc > 1)
	{
		m_window.set_FPS_limit(atoi(argv[1]));
	}
	else
	{
		if(SDL_GL_SetSwapInterval(1)!=0)
		{
			std::cout << "Error, swap interval not supported, " << SDL_GetError() << std::endl;
		}
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if(err!=GLEW_OK)
	{
		std::cout << "Aborting, GLEW initialization failed: " << glewGetErrorString(err) << std::endl;
		return 1;
	}

	vao_t vao;
	vao.bind();

	vbo_t vbo;
	vbo.bind();
	vbo.load_vertices("./Res/vertices");
	vbo.buffer_data(GL_STATIC_DRAW);

	ebo_t ebo;
	ebo.bind();
	ebo.load_elements("./Res/elements");
	ebo.buffer_data(GL_STATIC_DRAW);

	image_t tex({"Res/texture.png", "Res/test.png"});
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	GLuint vertexShader = load_shader("./Res/shader.vert", GL_VERTEX_SHADER);
	GLuint fragmentShader = load_shader("./Res/shader.frag", GL_FRAGMENT_SHADER);
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), 0);

	GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(2*sizeof(GLfloat)));

	GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
	glEnableVertexAttribArray(texAttrib);
	glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)(5*sizeof(GLfloat)));

	GLint texLocation = glGetUniformLocation(shaderProgram, "tex");
	

	uint32_t frames = 0;
	bool should_exit = false;
	SDL_Event windowEvent;
	auto start = std::chrono::high_resolution_clock::now();
	while(!should_exit)
	{
		while(SDL_PollEvent(&windowEvent))
		{
			if(windowEvent.type == SDL_QUIT || (windowEvent.type == SDL_KEYDOWN && windowEvent.key.keysym.sym == SDLK_ESCAPE))
			{
				should_exit = true;
			}
			if(windowEvent.type == SDL_MOUSEBUTTONDOWN)
			{
				SDL_SetRelativeMouseMode(SDL_TRUE);
				static bool test = 0;
				test = !test;
				glUniform1i(texLocation, test);
			}
			if(windowEvent.type == SDL_KEYDOWN && windowEvent.key.keysym.sym == SDLK_q)
			{
				SDL_SetRelativeMouseMode(SDL_FALSE);
			}
		}
		glFinish();
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		GLint err;
		if((err = glGetError())!=0)
		{
			std::cout << "glGetError() Returned: " << err << std::endl;
		}
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		m_window.swap_window();
		++frames;
	}
	std::chrono::duration<double> dur(std::chrono::high_resolution_clock::now() - start);
	start = std::chrono::high_resolution_clock::now();
	std::cout << "FPS: " << frames/dur.count() << std::endl;
	return 0;
}

