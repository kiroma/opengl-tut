#pragma once
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <chrono>

struct window_t
{
	window_t (uint32_t SDLFlags, const char* title, int width, int height, uint32_t windowFlags);
	~window_t();
	void swap_window();
	void set_FPS_limit(int new_limit);
private:
	int FPSLimit = 0;
	SDL_Window *win;
	SDL_GLContext context;
	decltype(std::chrono::high_resolution_clock::now()) time_t;
	std::chrono::duration<float> wait_time, frametime;
};
