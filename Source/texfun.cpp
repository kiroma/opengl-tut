#include "texfun.hpp"
#include <GL/glew.h>
#include <cstdio>
#include <cstdlib>

image_t::image_t(std::initializer_list<const char*> paths)
{
	// This function was originally written by David Grayson for
	// https://github.com/DavidEGrayson/ahrs-visualizer
	name.resize(paths.size());
	glGenTextures(paths.size(), name.data());
	unsigned int pass = 0;
	for(auto path : paths)
	{
		FILE *fp = fopen(path, "rb");
		if (fp == 0)
		{
			perror(path);
			abort_("Could not open file");
		}
		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if (!png_ptr)
		{
			abort_("creating read struct failed");
		}
		info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr)
		{
			png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
			abort_("Creating info struct failed");
		}
		// create png info struct
		end_info = png_create_info_struct(png_ptr);
		if (!end_info)
		{
			png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
			abort_("Creating endinfo struct failed");
		}
		
		// the code in this if statement gets called if libpng encounters an error
		if (setjmp(png_jmpbuf(png_ptr))) {
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			abort_("libPNG threw an error");
		}
		// init png reading
		png_init_io(png_ptr, fp);
		// read all the info up to the image data
		png_read_info(png_ptr, info_ptr);
		// get info about png
		png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);
		if (bit_depth != 8)
		{
			png_set_strip_16(png_ptr);
		}
		switch(color_type)
		{
			case PNG_COLOR_TYPE_RGB:
				format = GL_RGB;
				break;
			case PNG_COLOR_TYPE_RGB_ALPHA:
				format = GL_RGBA;
				break;
			default:
				fprintf(stderr, "libpng returned color_type=%d", color_type);
				abort_("libpng returned an unsupported color type");
		}
		// Update the png info struct.
		png_read_update_info(png_ptr, info_ptr);
		// Row size in bytes.
		int rowbytes = png_get_rowbytes(png_ptr, info_ptr);
		// glTexImage2d requires rows to be 4-byte aligned
		rowbytes += 3 - ((rowbytes-1) % 4);
		// Allocate the image_data as a big block, to be given to opengl
		image_data = (png_byte *)malloc(rowbytes * height * sizeof(png_byte)+15);
		if (image_data == NULL)
		{
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			fclose(fp);
			abort_("Allocating memory for PNG image data failed");
		}
		// row_pointers is for pointing to image_data for reading the png with libpng
		row_pointers = (png_byte **)malloc(height * sizeof(png_byte *));
		if (row_pointers == NULL)
		{
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			free(image_data);
			fclose(fp);
			abort_("Allocating memory for PNG row pointers failed");
		}
		// set the individual row_pointers to point at the correct offsets of image_data
		for (unsigned int i = 0; i < height; i++)
		{
			row_pointers[i] = image_data + i * rowbytes;
		}
		// read the png into image_data through row_pointers
		png_read_image(png_ptr, row_pointers);
		// Generate the OpenGL texture object
		glBindTexture(GL_TEXTURE_2D, name[pass]);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image_data);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		free(image_data);
		free(row_pointers);
		++pass;
	}
}


image_t::~image_t()
{
	glDeleteTextures(name.size(), name.data());
}

void image_t::abort_(const char* error_message)
{
	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
	printf("%s\n", error_message);
	throw;
}
