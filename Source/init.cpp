#include "init.hpp"
#include <iostream>
#include <thread>

window_t::window_t (uint32_t SDLFlags, const char* title, int width, int height, uint32_t windowFlags)
{
	SDL_Init(SDLFlags);
	this->win = SDL_CreateWindow(title, 0, 0, width, height, windowFlags);
	this->context = SDL_GL_CreateContext(this->win);
}

window_t::~window_t()
{
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(win);
	SDL_Quit();
}

void window_t::swap_window()
{
	if(FPSLimit)
	{
		frametime = std::chrono::high_resolution_clock::now() - time_t;
		if((wait_time = ((std::chrono::duration<float>(1)/FPSLimit)-frametime)).count() > 0)
		{
			std::this_thread::sleep_for(wait_time);
		}
		this->time_t = std::chrono::high_resolution_clock::now();
	}
	SDL_GL_SwapWindow(win);
}

void window_t::set_FPS_limit(int new_limit)
{
	this->FPSLimit = new_limit;
}
