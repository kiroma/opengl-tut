#pragma once
#include <GL/glew.h>
#include <string>
#include <vector>

GLuint load_shader(const char* shader_path, GLenum shaderType);

struct vao_t
{
	vao_t(GLsizei array_num = 1);
	vao_t(const vao_t&) = delete;
	vao_t(vao_t&& source);
	~vao_t();
	void bind();
private:
	GLuint buffenum;
	GLuint buffnum;
};

struct vbo_t
{
	vbo_t(GLsizei array_num = 1);
	vbo_t(const vbo_t&) = delete;
	vbo_t(vbo_t&& source);
	~vbo_t();
	void bind();
	void load_vertices(const char* path);
	void buffer_data(GLenum usage);
private:
	std::vector<GLfloat> vertices;
	GLuint buffenum;
	GLuint buffnum;
};

struct ebo_t
{
	ebo_t(GLsizei array_num = 1);
	ebo_t(const ebo_t&) = delete;
	ebo_t(ebo_t&& source);
	~ebo_t();
	void bind();
	void load_elements(const char* path);
	void buffer_data(GLenum usage);
private:
	std::vector<GLuint> elements;
	GLuint buffenum;
	GLuint buffnum;
};
